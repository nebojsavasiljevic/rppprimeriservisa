package rs.jep.rppprimeriservisa;

import java.io.StringReader;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint 
public class UvidPEEndpoint {
	  private static final String NAMESPACE_PEU = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/uvid";
	  private static final String NAMESPACE_PE = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S";
	  private static final String NAMESPACE_TNS = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis";

	  @PayloadRoot(namespace = NAMESPACE_TNS, localPart = "UvidPEMsg")  
	  @ResponsePayload
	  public Element handleUvidPeRequest(@RequestPayload Element uvidPEMsg)             
	      throws Exception {
		  
		Namespace nsPeu = Namespace.getNamespace("peu", NAMESPACE_PEU);
		
		Element uvidPE = uvidPEMsg.getChild("UvidPE", nsPeu);

		Element imeElement = uvidPE.getChild("UpitPE", nsPeu).getChild("ImeIliNaziv",nsPeu);  
		
		String responseXml = "<?xml version=\"1.0\"?>\r\n" + 
				"<UvidPEResponse xmlns:rpp=\"http://jep.rs/rpp/glavni/1.0-S\" xmlns=\"" + NAMESPACE_PE + "\">\r\n" + 
				"  <ZastevPrihvacen>true</ZastevPrihvacen>\r\n" + 
				"  <ListaUpisaPE>\r\n" + 
				"    <UpisPE>\r\n" + 
				"      <Id>1</Id>\r\n" + 
				"      <LiceId>70</LiceId>\r\n" + 
				"      <TipLica>FIZ</TipLica>\r\n" + 
				"      <PodaciOFizickomLicu>\r\n" + 
				"        <rpp:Ime>" + imeElement.getText() + "</rpp:Ime>\r\n" + 
				"        <rpp:Prezime>Петрпвски</rpp:Prezime>\r\n" + 
				"        <rpp:ImeRoditalja>Петар</rpp:ImeRoditalja>\r\n" + 
				"        <rpp:Pol>M</rpp:Pol>\r\n" + 
				"        <rpp:Jmbg>9549356931331</rpp:Jmbg>\r\n" + 
				"      </PodaciOFizickomLicu>\r\n" + 
				"      <DrzavaLica>\r\n" + 
				"        <rpp:Naziv>Македонија</rpp:Naziv>\r\n" + 
				"      </DrzavaLica>\r\n" + 
				"      <AdresaStanovanja>Македонија, PETROVEC, 14 40</AdresaStanovanja>\r\n" + 
				"      <BrojPredmeta>ПР-619/2014</BrojPredmeta>\r\n" + 
				"      <OdlukaOPrekrsaju>Осуђујућа пресуда</OdlukaOPrekrsaju>\r\n" + 
				"      <PravnaKvalifikacija> ч.331 с.1 т.10 - Закон о безбедности саобраћаја на путевима</PravnaKvalifikacija>\r\n" + 
				"      <KaznaNovcana>10000</KaznaNovcana>\r\n" + 
				"      <KaznaPoeni>0</KaznaPoeni>\r\n" + 
				"      <KaznaZastitneMere>Забрана управљања моторним возилом у трајању од 3. мес</KaznaZastitneMere>\r\n" + 
				"    </UpisPE>\r\n" + 
				"  </ListaUpisaPE>\r\n" + 
				"</UvidPEResponse>";
		
		SAXBuilder saxBuilder = new SAXBuilder();
		Element uvidPEResponse = saxBuilder
				.build(new StringReader(responseXml))
				.detachRootElement();
		
		Element uvidPEMsgResponse = new Element("UvidPEMsgResponse","tns",NAMESPACE_TNS);
		uvidPEMsgResponse.addContent(uvidPEResponse);
		
	    return uvidPEMsgResponse;
	  }
}
