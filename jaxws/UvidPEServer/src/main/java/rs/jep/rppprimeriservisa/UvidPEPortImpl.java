package rs.jep.rppprimeriservisa;

import javax.jws.WebService;

@WebService(endpointInterface = "rs.jep.rppprimeriservisa.UvidPEPortType", 
            targetNamespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")
public class UvidPEPortImpl implements UvidPEPortType {

	@Override
	public UvidPEMsgResponse uvidPE(UvidPEMsg inMsg) {

		String ime = inMsg.getUvidPE().getUpitPE().getImeIliNaziv();

		TUpisPE upisPE = new TUpisPE();
		upisPE.setLiceId(70);
		TOsnovniLicniPodaci olp = new TOsnovniLicniPodaci();
		olp.setIme(ime);
		olp.setPrezime("Петрпвски");
		olp.setImeRoditalja("Петар");
		olp.setPol(TPolLica.M);
		olp.setJmbg("9999999999999");
		upisPE.setPodaciOFizickomLicu(olp);
		TDrzava drzava = new TDrzava();
		drzava.setNaziv("Македонија");
		upisPE.setDrzavaLica(drzava);
		upisPE.setAdresaStanovanja("Скопље");
		upisPE.setPravnaKvalifikacija("ч.331 с.1 т.10 - Закон о безбедности саобраћаја на путевима");
		upisPE.setKaznaNovcana("10000");
		upisPE.setKaznaPoeni(0L);
		upisPE.setKaznaZastitneMere("Забрана управљања моторним возилом у трајању од 3. мес ");

		TUvidPEResponse uvidPEResponse = new TUvidPEResponse();
		uvidPEResponse.setZastevPrihvacen(true);
		TListaUpisaPE listaUpisaPE = new TListaUpisaPE();
		listaUpisaPE.getUpisPE().add(upisPE);
		uvidPEResponse.setListaUpisaPE(listaUpisaPE);
		UvidPEMsgResponse outMsg = new UvidPEMsgResponse();
		outMsg.setUvidPEResponse(uvidPEResponse);
		return outMsg;
	}

}
