package rs.jep.rppprimeriservisa;

/**
 * Hello world!
 *
 */
public class Main
{
    public static void main( String[] args )
    {
    	
//    	System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
//    	System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
//    	System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
//    	System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
    	
    	UvidPEPortImplService uvidPEClient = new UvidPEPortImplService();
    	UvidPEPortType uvidPEPort = uvidPEClient.getUvidPEPortImplPort();
    	
    	TUvidPE zahtev = new TUvidPE();
    	zahtev.setMatBrOrgPodnosioca("12345678");
    	zahtev.setNazivOrgPodnosioca("MP");
    	zahtev.setImeOvlLica("Marko");
    	zahtev.setPrezimeOvlLica("Marković");
    	zahtev.setJmbgOvlLica("0123456789012");
    	zahtev.setTipPravnogOsnova("član 999");
    	zahtev.setRadiPredmetaBroj("111/2011");
    	TUpitPE upitPE = new TUpitPE();
    	upitPE.setImeIliNaziv("+++ Petar +++");
    	zahtev.setUpitPE(upitPE);
    	UvidPEMsg inMsg = new UvidPEMsg();
    	inMsg.setUvidPE(zahtev);

    	UvidPEMsgResponse outMsg = uvidPEPort.uvidPE(inMsg);
    	
        System.out.println(outMsg.getUvidPEResponse().getListaUpisaPE().getUpisPE().get(0)
        		.getPodaciOFizickomLicu().getIme());
        System.out.println(outMsg.getUvidPEResponse().getListaUpisaPE().getUpisPE().get(0)
        		.getPodaciOFizickomLicu().getJmbg());
    }
}
