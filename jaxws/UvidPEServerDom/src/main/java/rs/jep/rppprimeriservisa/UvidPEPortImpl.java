package rs.jep.rppprimeriservisa;

import java.io.IOException;
import java.io.StringReader;

import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPElement;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@WebService(endpointInterface = "rs.jep.rppprimeriservisa.UvidPEPortType",
			targetNamespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")
public class UvidPEPortImpl implements UvidPEPortType {

	private static final String NAMESPACE_PEU = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/uvid";
	private static final String NAMESPACE_PE = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S";
	  
	@Override
	public UvidPEMsgResponse uvidPE(UvidPEMsg inMsg) {
		
        Element upit = (Element) inMsg.uvidPE.getElementsByTagNameNS(NAMESPACE_PEU, "UpitPE").item(0);
        
        NodeList l =  upit.getElementsByTagNameNS(NAMESPACE_PEU, "ImeIliNaziv");
        
        String ime = null;
        
        if (l.getLength() > 0)
        {
            ime = l.item(0).getTextContent();
        }
		String responseXml = "<?xml version=\"1.0\"?>\r\n" + 
				"<UvidPEResponse xmlns:rpp=\"http://jep.rs/rpp/glavni/1.0-S\" xmlns=\"" + NAMESPACE_PE + "\">\r\n" + 
				"  <ZastevPrihvacen>true</ZastevPrihvacen>\r\n" + 
				"  <ListaUpisaPE>\r\n" + 
				"    <UpisPE>\r\n" + 
				"      <Id>1</Id>\r\n" + 
				"      <LiceId>70</LiceId>\r\n" + 
				"      <TipLica>FIZ</TipLica>\r\n" + 
				"      <PodaciOFizickomLicu>\r\n" + 
				"        <rpp:Ime>" + ime + "</rpp:Ime>\r\n" + 
				"        <rpp:Prezime>Петрпвски</rpp:Prezime>\r\n" + 
				"        <rpp:ImeRoditalja>Петар</rpp:ImeRoditalja>\r\n" + 
				"        <rpp:Pol>M</rpp:Pol>\r\n" + 
				"        <rpp:Jmbg>9549356931331</rpp:Jmbg>\r\n" + 
				"      </PodaciOFizickomLicu>\r\n" + 
				"      <DrzavaLica>\r\n" + 
				"        <rpp:Naziv>Македонија</rpp:Naziv>\r\n" + 
				"      </DrzavaLica>\r\n" + 
				"      <AdresaStanovanja>Македонија, PETROVEC, 14 40</AdresaStanovanja>\r\n" + 
				"      <BrojPredmeta>ПР-619/2014</BrojPredmeta>\r\n" + 
				"      <OdlukaOPrekrsaju>Осуђујућа пресуда</OdlukaOPrekrsaju>\r\n" + 
				"      <PravnaKvalifikacija> ч.331 с.1 т.10 - Закон о безбедности саобраћаја на путевима</PravnaKvalifikacija>\r\n" + 
				"      <KaznaNovcana>10000</KaznaNovcana>\r\n" + 
				"      <KaznaPoeni>0</KaznaPoeni>\r\n" + 
				"      <KaznaZastitneMere>Забрана управљања моторним возилом у трајању од 3. мес</KaznaZastitneMere>\r\n" + 
				"    </UpisPE>\r\n" + 
				"  </ListaUpisaPE>\r\n" + 
				"</UvidPEResponse>";
        try {
             DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
             InputSource is = new InputSource();
             is.setCharacterStream(new StringReader(responseXml));
             Document outDocument = db.parse(is);
		     return new UvidPEMsgResponse(outDocument.getDocumentElement());
		} catch (ParserConfigurationException | SAXException | IOException   e) {
			throw new RuntimeException(e);
		}

	}

}
