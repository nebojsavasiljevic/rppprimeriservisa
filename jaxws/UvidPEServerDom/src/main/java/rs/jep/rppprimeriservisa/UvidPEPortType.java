
package rs.jep.rppprimeriservisa;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "UvidPEPortType", 
    targetNamespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface UvidPEPortType {
    @WebMethod(operationName = "UvidPE", 
       action = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis/UvidPE")
    @WebResult(name = "UvidPEMsgResponse", 
       targetNamespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis",
       partName = "parameters")
    public UvidPEMsgResponse uvidPE(
        @WebParam(name = "UvidPEMsg", 
            targetNamespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis", 
            partName = "parameters")
        UvidPEMsg parameters);
}
