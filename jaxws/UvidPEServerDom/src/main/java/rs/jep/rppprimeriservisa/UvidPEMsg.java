package rs.jep.rppprimeriservisa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;

import org.w3c.dom.Element;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TUpitPEMsg", 
    namespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")
public class UvidPEMsg {
	@XmlAnyElement
	public Element uvidPE;
}
