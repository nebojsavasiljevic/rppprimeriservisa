# Examples of XSD contract first SOAP web services implementation

This repository contains example projects that are explained in the Judicial Data Dictionary report, drafted under the "Judicial Efficiency Project in Serbia" (hereinafter: JEP) which is funded by the EU and implemented by the consortium led by the British Council.

The example projects are:

- .Net / WCF - server - full XML data binding ([wcf/UvidPEServer](wcf/UvidPEServer))
- .Net / WCF - server - XML DOM processing ([wcf/UvidPEServerDom](wcf/UvidPEServerDom))
- .Net / WCF - client ([wcf/UvidPEClient](wcf/UvidPEClient))
- Java / JAX-WS - server - full XML data binding ([jaxws/UvidPEServer](jaxws/UvidPEServer))
- Java / JAX-WS - server - XML DOM processing ([jaxws/UvidPEServerDom](jaxws/UvidPEServerDom))
- Java / JAX-WS - client ([jaxws/UvidPEClient](jaxws/UvidPEClient))
- Java / Spring-WS - server - XML DOM processing ([springws/uvidPEServerDom](springws/uvidPEServerDom))

Autor: Nebojša Vasiljeivć
