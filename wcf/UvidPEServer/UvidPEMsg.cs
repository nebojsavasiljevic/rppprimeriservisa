﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace UvidPEServer
{
    [MessageContract]
    public class UvidPEMsg 
    {
        [XmlElement(Namespace = 
            "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/uvid")]
        [MessageBodyMember]
        public TUvidPE UvidPE;
    }
}