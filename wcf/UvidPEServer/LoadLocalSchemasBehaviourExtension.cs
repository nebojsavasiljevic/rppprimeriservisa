﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace UvidPEServer
{
    public class LoadLocalSchemasBehaviourExtension : BehaviorExtensionElement
    {
        [ConfigurationProperty("xsdFolder")]
        public string XsdFolder
        {
            get
            {
                object value = this["xsdFolder"];
                return value != null ? value.ToString() : string.Empty;
            }
            set { this["xsdFolder"] = value; }
        }

        public override Type BehaviorType
        {
            get { return typeof(LoadLocalSchemasBehavior); }
        }
    
        protected override object CreateBehavior()
        {
            return new LoadLocalSchemasBehavior(XsdFolder);
        }
    }
}