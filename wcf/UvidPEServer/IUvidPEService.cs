﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UvidPEServer
{
    [ServiceContract(Namespace = 
        "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")]
    [XmlSerializerFormat]
    public interface IUvidPEService
    {
        [OperationContract]
        [XmlSerializerFormat]
        UvidPEMsgResponse UvidPE(UvidPEMsg inMsg);
    }
}
