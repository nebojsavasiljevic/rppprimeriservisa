﻿using System.Collections.Generic;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml.Schema;
using System.Web;
using System.Linq;
using System.IO;

namespace UvidPEServer
{
    public class LoadLocalSchemasBehavior
       : IEndpointBehavior, IWsdlExportExtension
    {
        private XmlSchemaSet localSchemaSet;
        private List<XmlSchema> localSchemaList;

        // u konkstuktoru se učitavaju sve sheme iz xsdFolder
        public LoadLocalSchemasBehavior(string xsdFolder)
        {
            List<string> localSchemaFiles = Directory.EnumerateFiles(
                HttpContext.Current.Server.MapPath(xsdFolder),
                "*.xsd", SearchOption.AllDirectories).ToList();
            localSchemaSet = new XmlSchemaSet();
            // localSchemaSet.ValidationEventHandler += new ValidationEventHandler(ValidationCallback);
            localSchemaList = new List<XmlSchema>();
            foreach (string scmFileName in localSchemaFiles)
            {
                string fn = scmFileName;
                var schema = localSchemaSet.Add(null, fn);

                // brisu se SchemaLocation podaci iz include-ova da bi se ostavilo da
                // WCF automatski dodeli SchemaLocation
                foreach (var scmItem in schema.Includes)
                    if (scmItem is XmlSchemaImport)
                        (scmItem as XmlSchemaImport).SchemaLocation = null;

                localSchemaList.Add(schema);
            }
            localSchemaSet.Compile();
        }

        void IWsdlExportExtension.ExportEndpoint(WsdlExporter exporter,
              WsdlEndpointConversionContext context)
        {
            XmlSchemaSet generatedSchemaSet = exporter.GeneratedXmlSchemas;

            foreach (XmlSchema localSchema in localSchemaList)
            {
                if (generatedSchemaSet.Contains(localSchema.TargetNamespace))
                {
                    // ako šema sa istim namespaceom postoji, briše se jer 
                    // se zamenjuje lokalnom (API dozvoljava više šema sa istim
                    // namespace-om, pa smo pokrili i takav slučaj)
                    List<XmlSchema> toRemove = new List<XmlSchema>();
                    foreach (XmlSchema sch in generatedSchemaSet.Schemas(localSchema.TargetNamespace))
                        toRemove.Add(sch);
                    foreach(XmlSchema sch in toRemove)
                        generatedSchemaSet.Remove(sch);
                }
                
                generatedSchemaSet.Add(localSchema);
            }
            generatedSchemaSet.Compile();
          
        }
        void IWsdlExportExtension.ExportContract(WsdlExporter exporter,
              WsdlContractConversionContext context)
        {
            // Nikad se ne poziva jer nije implementira IContractBehavior
        }
        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, 
            ClientRuntime clientRuntime)
        {
            // ne koristi se
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, 
            EndpointDispatcher endpointDispatcher)
        {
            // ne koristi se
        }
        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, 
            BindingParameterCollection bindingParameters)
        {
            // ne koristi se
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        {
            // ne koristi se
        }
    }





}