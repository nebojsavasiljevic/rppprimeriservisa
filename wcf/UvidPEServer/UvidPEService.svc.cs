﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UvidPEServer
{
    public class UvidPEService : IUvidPEService
    {
        public UvidPEMsgResponse UvidPE(UvidPEMsg inMsg)
        {
            string ime = inMsg.UvidPE.UpitPE.ImeIliNaziv;
            var outMsg = new UvidPEMsgResponse
            {
                UvidPEResponse = new TUvidPEResponse
                {
                    ZastevPrihvacen = true,
                    ListaUpisaPE = new TListaUpisaPE
                    {
                        UpisPE = new TUpisPE[]
                        {
                            new TUpisPE {
                                Id = 1,
                                LiceId = 70,
                                PodaciOFizickomLicu = new TOsnovniLicniPodaci
                                {
                                    Ime = ime,
                                    Prezime = "Петрпвски",
                                    ImeRoditalja = "Петар",
                                    Pol = TPolLica.M,
                                    Jmbg = "9999999999999"
                                },
                                DrzavaLica = new TDrzava { Naziv = "Македонија" },
                                AdresaStanovanja = "Скопље",
                                PravnaKvalifikacija = "ч.331 с.1 т.10 - Закон о безбедности саобраћаја на путевима",
                                KaznaNovcana = "10000",
                                KaznaPoeni = 0,
                                KaznaPoeniSpecified = true,
                                KaznaZastitneMere = "Забрана управљања моторним возилом у трајању од 3. мес "
                            }
                        }
                    }
                }
            };

            return outMsg;

        }
    }
}
