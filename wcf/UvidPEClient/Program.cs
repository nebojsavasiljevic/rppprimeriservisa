﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UvidPEClient.UvidPEServiceReference;

namespace UvidPEClient
{
    class Program
    {
        static void Main(string[] args)
        {
            UvidPEServiceClient client = new UvidPEServiceClient();
            //client.Endpoint.Address = 
            //    new System.ServiceModel.EndpointAddress("http://localhost:55924/UvidPEService.svc");
            TUvidPE zahtev = new TUvidPE
            {
                MatBrOrgPodnosioca = "12345678",
                NazivOrgPodnosioca = "MP",
                ImeOvlLica = "Marko",
                PrezimeOvlLica = "Marković",
                JmbgOvlLica = "0123456789012",
                TipPravnogOsnova = "član 999",
                RadiPredmetaBroj = "111/2011",
                UpitPE = new TUpitPE
                {
                    ImeIliNaziv = "+++ Petar +++"
                }
            };
            var odgovor = client.UvidPE(zahtev);
            Console.WriteLine(odgovor.ListaUpisaPE.UpisPE[0].PodaciOFizickomLicu.Ime);
            Console.WriteLine(odgovor.ListaUpisaPE.UpisPE[0].PodaciOFizickomLicu.Jmbg);
            client.Close();
        }
    }
}
