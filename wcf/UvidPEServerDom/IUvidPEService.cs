﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace UvidPEServerDom
{
    [ServiceContract(Namespace = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/servis")]
    [XmlSerializerFormat]
    public interface IUvidPEService
    {

        [OperationContract]
        [XmlSerializerFormat]
        UvidPEMsgResponse UvidPE(UvidPEMsg bt);

    }
}
