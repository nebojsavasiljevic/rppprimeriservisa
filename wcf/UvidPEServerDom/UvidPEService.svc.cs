﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace UvidPEServerDom
{
    public class UvidPEService : IUvidPEService
    {
        const string uvidNs = "http://jep.rs/rpp/prekrsajna-evidencija/1.0-S/uvid";
        public UvidPEMsgResponse UvidPE(UvidPEMsg inMsg)
        {
            
            XmlElement upit = inMsg.element.GetElementsByTagName("UpitPE", uvidNs)[0] as XmlElement;
            string ime = null;
            var l = upit.GetElementsByTagName("ImeIliNaziv", uvidNs);
            
            if (l.Count > 0)
            {
                ime = l[0].InnerText;
            }

            XmlDocument outDocument = new XmlDocument();
            outDocument.InnerXml = $@"<?xml version=""1.0""?>
<UvidPEResponse xmlns:rpp=""http://jep.rs/rpp/glavni/1.0-S"" xmlns=""http://jep.rs/rpp/prekrsajna-evidencija/1.0-S""
                >
  <ZastevPrihvacen>true</ZastevPrihvacen>
  <ListaUpisaPE>
    <UpisPE>
      <Id>1</Id>
      <LiceId>70</LiceId>
      <TipLica>FIZ</TipLica>
      <PodaciOFizickomLicu>
        <rpp:Ime>{ime}</rpp:Ime>
        <rpp:Prezime>Петрпвски</rpp:Prezime>
        <rpp:ImeRoditalja>Петар</rpp:ImeRoditalja>
        <rpp:Pol>M</rpp:Pol>
        <rpp:Jmbg>9549356931331</rpp:Jmbg>
      </PodaciOFizickomLicu>
      <DrzavaLica>
        <rpp:Naziv>Македонија</rpp:Naziv>
      </DrzavaLica>
      <AdresaStanovanja>&lt;b&gt;Адреса пребивалишта&lt;/b&gt;: Македонија, PETROVEC, 14 40</AdresaStanovanja>
      <BrojPredmeta>ПР-619/2014</BrojPredmeta>
      <OdlukaOPrekrsaju>Осуђујућа пресуда</OdlukaOPrekrsaju>
      <PravnaKvalifikacija> ч.331 с.1 т.10 - &lt;b&gt;Закон о безбедности саобраћаја на путевима&lt;/b&gt;</PravnaKvalifikacija>
      <KaznaNovcana>10000</KaznaNovcana>
      <KaznaPoeni>0</KaznaPoeni>
      <KaznaZastitneMere>Забрана управљања моторним возилом у трајању од 3. мес </KaznaZastitneMere>
    </UpisPE>
  </ListaUpisaPE>
</UvidPEResponse>";

            return new UvidPEMsgResponse(outDocument.DocumentElement);
        }
    }
}
